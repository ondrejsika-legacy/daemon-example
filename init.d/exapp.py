#!/usr/bin/python

from libdaemon import Daemon

daemon = Daemon("exappd", "> tmp/exappd.txt", "run/exappd.pid", "./bin")

if __name__ == '__main__':
    import sys
    daemon.command_line(sys.argv)
