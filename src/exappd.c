#include <stdio.h>
#include <unistd.h>

int main(){
  int i = 0;
  while(1){
    fprintf(stdout, "%d\n", i);
    setvbuf(stdout, NULL, _IONBF, 0);
    sleep(1);
    i++;
  }
  return 0;
}
